package com.example.foranythingwith1activity;

import android.graphics.drawable.AnimationDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText et;
    private TextView tv;

    private TextWatcher textwatching = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



//        ConstraintLayout constraintLayout = findViewById(R.id.backgroundpicture);
//        AnimationDrawable animationDrawable = (AnimationDrawable) constraintLayout.getBackground();
//        animationDrawable.setEnterFadeDuration(1500);
//        animationDrawable.setExitFadeDuration(1500);
//        animationDrawable.start();


        et = findViewById(R.id.GetCarPlate);
        tv = findViewById(R.id.ViewCarPlate);


        textwatching = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                et = findViewById(R.id.GetCarPlate);
                tv = findViewById(R.id.ViewCarPlate);
                try {
                    String letters[] = {"-", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
                    String checksum[] = {"A", "Z", "Y", "X", "U", "T", "S", "R", "P", "M", "L", "K", "J", "H", "G", "E", "D", "C", "B"};
                    String plated = et.getText().toString();
                    char[] pNumber = plated.toCharArray();

                    int num1 = 0;
                    int num2 = 0;
                    int fullnum =0;

                    if (pNumber[0] == 's' | pNumber[0] == 'S' | pNumber[0] == 'g' | pNumber[0] == 'G') {
                        num1 = "-ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(pNumber[1]);
                        num2 = "-ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(pNumber[2]);

                        fullnum += (num1 * 9);
                        fullnum += (num2 * 4);
                        fullnum += (Character.getNumericValue(pNumber[3]) * 5);
                        fullnum += (Character.getNumericValue(pNumber[4]) * 4);
                        fullnum += (Character.getNumericValue(pNumber[5]) * 3);
                        fullnum += (Character.getNumericValue(pNumber[6]) * 2);

                        int lastletterindex = fullnum % 19;
                        String finalchar = checksum[lastletterindex];
                        String newtext = (et.getText().toString() + finalchar).trim();
                        tv.setText(newtext.toUpperCase());
                    }
                    else {
                            num1 = "-ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(pNumber[0]);
                            num2 = "-ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(pNumber[1]);

                            fullnum += (num1 * 9);
                            fullnum += (num2 * 4);
                            fullnum += (Character.getNumericValue(pNumber[2]) * 5);
                            fullnum += (Character.getNumericValue(pNumber[3]) * 4);
                            fullnum += (Character.getNumericValue(pNumber[4]) * 3);
                            fullnum += (Character.getNumericValue(pNumber[5]) * 2);

                            int lastletterindex = fullnum % 19;
                            String finalchar = checksum[lastletterindex];

                            String newtext = (et.getText().toString().toUpperCase() + finalchar).trim();
                            tv.setText(newtext.toUpperCase());
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                    Log.e("ERROR", e.toString());
                    Toast.makeText(getApplicationContext(), '?', Toast.LENGTH_SHORT).show();
                }
            }
        };
        et.addTextChangedListener(textwatching);
    }
    public void CheckPlate(View v){

//        EditText et;
//        TextView tv;


    }

}


//    EditText editText = findViewById(R.id.GetCarPlate);
//    editText.setOnEditorActionListener(new OnEditorActionListener() {
//@Override
//public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//        boolean handled = false;
//        if (actionId == EditorInfo.IME_ACTION_SEND) {
//        sendMessage();
//        handled = true;
//        }
//        return handled;
//        }
//        });